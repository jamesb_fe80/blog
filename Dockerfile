FROM ruby:latest

RUN gem install jekyll \
    && gem install jekyll-asciidoc \
    && gem install jekyll-gzip
